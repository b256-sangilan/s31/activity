/*

1. What directive is used by Node.js in loading the modules it needs?

Answer:
*require();
*'Require directive' is used to load a Node module(http) and store returned its instance(http) into its variable(http).

2. What Node.js module contains a method for server creation?
Answer:
 'http module' 


3. What is the method of the http object responsible for creating a server using Node.js?

Answer:
 'createServer'

4. What method of the response object allows us to set status codes and content types?

Answer:
	'status method'

5. Where will console.log() output its contents when run in Node.js?

Answer:
	'console.log() output will show on the terminal'

6. What property of the request object contains the address's endpoint?
Answer:
	'end() is a method that ends our response. No more tasks will run after this code.'


*/